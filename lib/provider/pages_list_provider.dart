part of 'providers.dart';

final pageListProvider = Provider((ref) => [
      const HomePage(),
      const SettingsPage(),
    ]);

final pageIndexProvider = StateProvider<int>((ref) => 0);
