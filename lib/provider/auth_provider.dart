part of 'providers.dart';

enum AuthStatus {
  initial,
  signed,
  error,
}

final authStatus = Provider((ref) => AuthStatus.initial);
