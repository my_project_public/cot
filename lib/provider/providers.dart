import 'package:control_of_talk/ui/pages/pages.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

part 'pages_list_provider.dart';
part 'auth_provider.dart';
part 'vote_provider.dart';
