part of 'widgets.dart';

class OverviewCards extends StatelessWidget {
  final Color color;
  final Widget icon;
  final String title;
  final String total;
  const OverviewCards({
    Key? key,
    required this.color,
    required this.title,
    required this.total,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      height: 150,
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: color,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon,
          Text(
            total,
            style: whiteTextStyle.copyWith(fontSize: 28),
          ),
          Text(
            title,
            style: whiteTextStyle,
          ),
        ],
      ),
    );
  }
}
