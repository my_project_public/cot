part of 'widgets.dart';

class CustomTextField extends StatelessWidget {
  final String placeholder;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Color? color;
  final TextInputType keyType;
  final bool obscure;
  final Function(String)? onChange;

  const CustomTextField({
    Key? key,
    required this.placeholder,
    this.prefixIcon,
    this.suffixIcon,
    this.obscure = false,
    this.onChange,
    this.keyType = TextInputType.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      style: blackTextStyle,
      keyboardType: keyType,
      onChanged: onChange,
      decoration: InputDecoration(
        fillColor: color,
        filled: true,
        hintText: placeholder,
        hintStyle: greyTextStyle,
        prefixIcon: Padding(
          padding: const EdgeInsets.only(left: 16, right: 8),
          child: prefixIcon,
        ),
        suffixIcon: Padding(
          padding: const EdgeInsets.only(right: 16),
          child: suffixIcon,
        ),
        border: OutlineInputBorder(
          // width: 0.0 produces a thin "hairline" border
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide.none,
          //borderSide: const BorderSide(),
        ),
      ),
    );
  }
}
