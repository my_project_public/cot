part of 'widgets.dart';

class RiwayatTile extends StatelessWidget {
  final Riwayat riwayat;
  const RiwayatTile({Key? key, required this.riwayat}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 16),
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: whiteColor,
        boxShadow: [
          BoxShadow(
            color: secondaryColor.withOpacity(0.2),
            blurRadius: 24,
            offset: const Offset(0, 8),
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ImageIcon(
            const AssetImage('assets/icon_profile.png'),
            color: secondaryColor,
          ),
          const SizedBox(
            width: 16,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                (riwayat.createdAt == null)
                    ? "null"
                    : DateFormat.yMMMd().format(riwayat.createdAt!),
                style: greyTextStyle.copyWith(fontSize: 12, fontWeight: light),
              ),
              Text(
                "${riwayat.qty} Orang",
                style:
                    blackTextStyle.copyWith(fontSize: 18, fontWeight: medium),
              ),
            ],
          ),
          const Spacer(),
          Icon(
            Icons.arrow_forward_ios_rounded,
            color: darkGreyColor,
          )
        ],
      ),
    );
  }
}
