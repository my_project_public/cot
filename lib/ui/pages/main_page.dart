part of 'pages.dart';

class MainPage extends ConsumerWidget {
  static const route = "main_page";
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: Stack(
        children: [
          buildBody(ref),
          buildBottomNavBar(ref),
        ],
      ),
    );
  }

  Widget buildBody(WidgetRef ref) {
    final int index = ref.watch(pageIndexProvider);
    return ref.read(pageListProvider)[index];
  }

  Widget buildBottomNavBar(WidgetRef ref) {
    final int index = ref.watch(pageIndexProvider);

    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        margin: const EdgeInsets.only(bottom: 30, left: edge, right: edge),
        height: 65,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24),
          color: lightGreyColor,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    ref.read(pageIndexProvider.notifier).state = 0;
                  },
                  child: ImageIcon(
                    const AssetImage('assets/icon_home.png'),
                    color: index == 0 ? secondaryColor : darkGreyColor,
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                index == 0
                    ? Container(
                        height: 4,
                        width: 30,
                        decoration: BoxDecoration(
                          color: secondaryColor,
                          borderRadius: BorderRadius.circular(1000),
                        ),
                      )
                    : Container(),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    ref.read(pageIndexProvider.notifier).state = 1;
                  },
                  child: ImageIcon(
                    const AssetImage('assets/icon_settings.png'),
                    color: index == 1 ? secondaryColor : darkGreyColor,
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                index == 1
                    ? Container(
                        height: 4,
                        width: 30,
                        decoration: BoxDecoration(
                          color: secondaryColor,
                          borderRadius: BorderRadius.circular(1000),
                        ),
                      )
                    : Container(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
