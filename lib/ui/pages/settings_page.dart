part of 'pages.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.only(top: 28),
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            divider("Settings"),
            Container(
              margin:
                  const EdgeInsets.only(top: 12, left: edge + 8, right: edge),
              child: InkWell(
                onTap: () {},
                child: Text(
                  "Reset Password",
                  style: blackTextStyle,
                ),
              ),
            ),
            Container(
              margin:
                  const EdgeInsets.only(top: 12, left: edge + 8, right: edge),
              child: InkWell(
                onTap: () {},
                child: Text(
                  "Logout",
                  style: blackTextStyle.copyWith(color: Colors.red[300]),
                ),
              ),
            ),
            divider("More Info"),
            Container(
              margin:
                  const EdgeInsets.only(top: 12, left: edge + 8, right: edge),
              child: InkWell(
                onTap: () {},
                child: Text(
                  "About Application",
                  style: blackTextStyle,
                ),
              ),
            ),
            Container(
              margin:
                  const EdgeInsets.only(top: 12, left: edge + 8, right: edge),
              child: InkWell(
                onTap: () {},
                child: Text(
                  "About Developer",
                  style: blackTextStyle,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget divider(String title) => Container(
        margin: const EdgeInsets.only(top: 12, left: edge, right: edge),
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        width: double.infinity,
        height: 28,
        color: lightGreyColor,
        child: Text(
          title,
          style: blackTextStyle,
        ),
      );
}
