part of 'pages.dart';

class LoginPage extends StatelessWidget {
  static const route = "login_page";
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            welcomeImage(),
            titleText(),
            emailLabel(),
            emailTextField(),
            passwordLabel(),
            passwordTextField(),
            submitButton(),
          ],
        ),
      ),
    );
  }

  Widget welcomeImage() => Container(
        height: 288,
        width: double.infinity,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('welcome_image.png'),
            fit: BoxFit.cover,
          ),
        ),
      );

  Widget titleText() => Container(
        margin: const EdgeInsets.only(top: 48, left: edge, right: edge),
        child: Text(
          "Login",
          style: blackTextStyle.copyWith(fontSize: 28, fontWeight: bold),
        ),
      );

  Widget emailLabel() => Container(
        margin: const EdgeInsets.only(top: 38, left: edge, right: edge),
        child: Text("Email address", style: blackTextStyle),
      );

  Widget emailTextField() => Container(
        margin: const EdgeInsets.only(top: 12, left: edge, right: edge),
        child: CustomTextField(
          placeholder: "Your email here...",
          onChange: (value) {},
          color: lightGreyColor,
          prefixIcon: const ImageIcon(
            AssetImage('assets/icon_message.png'),
          ),
        ),
      );

  Widget passwordLabel() => Container(
        margin: const EdgeInsets.only(top: 12, left: edge, right: edge),
        child: Text("Password", style: blackTextStyle),
      );

  Widget passwordTextField() => Container(
        margin: const EdgeInsets.only(top: 12, left: edge, right: edge),
        child: CustomTextField(
          obscure: true,
          placeholder: "Your password here...",
          color: lightGreyColor,
          onChange: (value) {},
          prefixIcon: const ImageIcon(
            AssetImage('assets/icon_lock.png'),
          ),
        ),
      );

  Widget submitButton() => Container(
        margin: const EdgeInsets.only(
            top: 32, left: edge, right: edge, bottom: 100),
        child: CustomButton(onTap: () {}, title: "Login"),
      );
}
