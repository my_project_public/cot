part of 'pages.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(),
            overview(),
            menu(context),
            const SizedBox(
              height: 100,
            )
          ],
        ),
      ),
    );
  }

  Widget header() => Container(
        margin: const EdgeInsets.only(top: 40, left: edge, right: edge),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "CoT System",
              style: blackTextStyle.copyWith(
                fontSize: 24,
                fontWeight: semiBold,
              ),
            ),
            Text(
              "Control of Talk System",
              style: blackTextStyle.copyWith(
                fontWeight: regular,
              ),
            )
          ],
        ),
      );

  Widget overview() => Container(
        margin: const EdgeInsets.only(top: 32, left: edge, right: edge),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            OverviewCards(
                icon: ImageIcon(
                  const AssetImage('assets/icon_piechart.png'),
                  color: whiteColor,
                ),
                color: blueColor,
                title: "Suara bulan ini",
                total: "42"),
            OverviewCards(
              icon: ImageIcon(
                const AssetImage('assets/icon_piechart2.png'),
                color: whiteColor,
              ),
              color: lightBlueColor,
              title: "Total suara",
              total: "100000",
            )
          ],
        ),
      );

  Widget menu(BuildContext context) => Container(
        margin: const EdgeInsets.only(top: 32, left: edge, right: edge),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Menu",
              style: blackTextStyle.copyWith(
                fontSize: 20,
                fontWeight: semiBold,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            MenuBtn(
              onTap: () {
                Navigator.pushNamed(context, FormPage.route);
              },
              title: "Vote Suara",
              icon: ImageIcon(
                const AssetImage('assets/icon_send.png'),
                size: 40,
                color: whiteColor,
              ),
              color: primaryColor,
            ),
            const SizedBox(
              height: 16,
            ),
            MenuBtn(
              onTap: () {
                Navigator.pushNamed(context, RiwayatPage.route);
              },
              title: "Lihat Riwayat",
              icon: ImageIcon(
                const AssetImage('assets/icon_clock.png'),
                size: 40,
                color: whiteColor,
              ),
              color: secondaryColor,
            )
          ],
        ),
      );
}

class MenuBtn extends StatelessWidget {
  final String title;
  final VoidCallback onTap;
  final Color color;
  final Widget icon;
  const MenuBtn({
    Key? key,
    required this.title,
    required this.icon,
    required this.color,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.only(bottom: 24),
        width: double.infinity,
        height: 80,
        padding: const EdgeInsets.all(edge),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              color,
              color.withOpacity(0.2),
            ],
            begin: const Alignment(0.5, 0.2),
            end: const Alignment(0.5, 1.5),
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          children: [
            icon,
            const SizedBox(
              width: 16,
            ),
            Text(
              title,
              style: whiteTextStyle.copyWith(
                fontSize: 20,
                fontWeight: bold,
              ),
            )
          ],
        ),
      ),
    );
  }
}
