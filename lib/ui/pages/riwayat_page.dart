part of 'pages.dart';

class RiwayatPage extends StatelessWidget {
  static const route = "riwayat";
  const RiwayatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              title(),
              overview(),
              listRiwayat(),
            ],
          ),
        ),
      ),
    );
  }

  Widget title() => Container(
        margin: const EdgeInsets.only(top: 40, left: edge, right: edge),
        child: Text(
          "Riwayat Suara",
          style: blackTextStyle.copyWith(fontSize: 24, fontWeight: semiBold),
        ),
      );
  Widget overview() => Container(
        margin: const EdgeInsets.only(top: 32, left: edge, right: edge),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            OverviewCards(
                icon: ImageIcon(
                  const AssetImage('assets/icon_piechart.png'),
                  color: whiteColor,
                ),
                color: blueColor,
                title: "Suara bulan ini",
                total: "42"),
            OverviewCards(
              icon: ImageIcon(
                const AssetImage('assets/icon_piechart2.png'),
                color: whiteColor,
              ),
              color: lightBlueColor,
              title: "Total suara",
              total: "100000",
            )
          ],
        ),
      );

  Widget listRiwayat() => Container(
        margin: const EdgeInsets.only(top: 32, left: edge, right: edge),
        child: Column(
          children: dummyHistory.map((e) => RiwayatTile(riwayat: e)).toList(),
        ),
      );
}
