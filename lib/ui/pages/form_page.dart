part of 'pages.dart';

class FormPage extends StatelessWidget {
  static const route = "form_page";
  const FormPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              title(),
              bannerImage(),
              form(),
              submitButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget title() => Container(
        margin: const EdgeInsets.only(top: 40, left: edge, right: edge),
        child: Text(
          "Tambah Suara",
          style: blackTextStyle.copyWith(fontSize: 24, fontWeight: semiBold),
        ),
      );

  Widget bannerImage() => Container(
        margin: const EdgeInsets.only(top: 16, left: edge, right: edge),
        width: double.infinity,
        height: 218,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          image: const DecorationImage(
            image: AssetImage('assets/vote_image.png'),
            fit: BoxFit.cover,
          ),
        ),
      );

  Widget form() => Container(
        margin: const EdgeInsets.only(top: 32, left: edge, right: edge),
        padding: const EdgeInsets.all(16),
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: lightGreyColor,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            totalLabel(),
            totalTextField(),
          ],
        ),
      );

  Widget totalLabel() => Text("Jumlah Suara", style: blackTextStyle);

  Widget totalTextField() => Container(
        margin: const EdgeInsets.only(top: 4),
        child: CustomTextField(
          placeholder: "Input a number",
          color: greyColor,
          onChange: (value) {},
          keyType: TextInputType.number,
          prefixIcon: Text(
            '#',
            style: blackTextStyle.copyWith(
              fontSize: 24,
              height: 1.5,
              fontWeight: medium,
            ),
          ),
        ),
      );

  Widget submitButton() => Container(
        margin: const EdgeInsets.only(
            top: 32, left: edge, right: edge, bottom: 100),
        child: CustomButton(
          onTap: () {},
          title: "Submit",
        ),
      );
}
