import 'package:control_of_talk/provider/providers.dart';
import 'package:control_of_talk/shared/theme.dart';
import 'package:control_of_talk/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/models.dart';

part 'login_page.dart';
part 'main_page.dart';
part 'home_page.dart';
part 'settings_page.dart';
part 'form_page.dart';
part 'riwayat_page.dart';
