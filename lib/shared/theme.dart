import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const double edge = 24;

Color primaryColor = const Color(0xffF95A37);
Color secondaryColor = const Color(0xff415380);
Color greyColor = const Color(0xffE4E4E4);
Color darkGreyColor = const Color(0xff828282);
Color lightGreyColor = const Color(0xffF2F2F2);
Color whiteColor = const Color(0xffFFFFFF);
Color blackColor = const Color(0xff111622);
Color lightGreenColor = const Color(0xffAEE5D1);
Color blueColor = const Color(0xff0080F6);
Color lightBlueColor = const Color(0xff5CB1FF);

TextStyle blackTextStyle = GoogleFonts.nunito(color: blackColor);
TextStyle greyTextStyle = GoogleFonts.nunito(color: darkGreyColor);
TextStyle whiteTextStyle = GoogleFonts.nunito(color: whiteColor);
TextStyle greenTextStyle = GoogleFonts.nunito(color: lightGreenColor);

FontWeight bold = FontWeight.w700;
FontWeight semiBold = FontWeight.w600;
FontWeight medium = FontWeight.w500;
FontWeight regular = FontWeight.w400;
FontWeight light = FontWeight.w300;
