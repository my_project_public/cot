part of 'services.dart';

class UserService {
  Future<ApiReturnValue<User>> login(String email, String password,
      {http.Client? client}) async {
    client ??= http.Client();

    String url = baseUrl + "api/login";
    var uri = Uri.parse(url);

    final response = await http.post(uri, headers: {
      "Content-type": "application/json"
    }, body: {
      jsonEncode(<String, String>{'email': email, 'password': password})
    });

    if (response.statusCode != 200) {
      return ApiReturnValue(message: "Please try again");
    }

    var data = jsonDecode(response.body);

    User.token = data["result"]["token"];
    User value = User.fromJson(data["result"]["user"]);

    return ApiReturnValue(value: value);
  }
}
