part of 'services.dart';

class VoteServices {
  Future<ApiReturnValue<LaporanResponseModel>> getHistory(
      {http.Client? client}) async {
    client ??= http.Client();

    String url = baseUrl + 'api/login';
    var uri = Uri.parse(url);

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      return ApiReturnValue(message: "Please try again");
    }

    var data = jsonDecode(response.body);

    LaporanResponseModel results = LaporanResponseModel.fromJson(data);
    return ApiReturnValue(value: results);
  }

  Future<ApiReturnValue<Riwayat>> addSuara(int total,
      {http.Client? client}) async {
    client ??= http.Client();

    String url = baseUrl + 'api/laporan/store';
    var uri = Uri.parse(url);

    var response = await client.post(
      uri,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer ${User.token}"
      },
      body: jsonEncode(
        <String, dynamic>{
          'qty': total,
        },
      ),
    );

    if (response.statusCode != 200) {
      return ApiReturnValue(message: "Please try again");
    }

    var data = jsonDecode(response.body);

    Riwayat riwayat = Riwayat.fromJson(data['result']);

    return ApiReturnValue(value: riwayat);
  }
}
