import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'ui/pages/pages.dart';

void main() {
  runApp(ProviderScope(child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Control of Talk',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        LoginPage.route: (context) => const LoginPage(),
        FormPage.route: (context) => const FormPage(),
        MainPage.route: (context) => const MainPage(),
        RiwayatPage.route: (context) => const RiwayatPage(),
      },
      initialRoute: MainPage.route,
    );
  }
}
