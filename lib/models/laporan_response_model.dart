part of 'models.dart';

class LaporanResponseModel {
  num? totalSuara;
  num? bulanKemarin;
  num? bulanSekarang;
  List<Riwayat>? laporan;

  LaporanResponseModel({
    this.totalSuara,
    this.bulanKemarin,
    this.bulanSekarang,
    this.laporan,
  });

  Map<String, dynamic> toJson() => {
        'total_suara': totalSuara,
        'bulan_kemarin': bulanKemarin,
        'bulan_sekarang': bulanSekarang,
      };

  factory LaporanResponseModel.fromJson(Map<String, dynamic> data) =>
      LaporanResponseModel(
        totalSuara: data['total_suara'] as num,
        bulanKemarin: data['bulan_kemarin'] as num,
        bulanSekarang: data['bulan_sekarang'] as num,
        laporan: (data['result']['laporan'] as Iterable)
            .map((e) => Riwayat.fromJson(data))
            .toList(),
      );
}
