part of 'models.dart';

class Riwayat {
  int? id;
  int? kecamatanId;
  num? qty;
  DateTime? createdAt;
  Riwayat({
    this.id,
    this.kecamatanId,
    this.qty,
    this.createdAt,
  });

  Map<String, dynamic> toJson() => {
        'id': id,
        'kecamatan_id': kecamatanId,
        'qty': qty,
        'created_at': createdAt,
      };

  factory Riwayat.fromJson(Map<String, dynamic> data) => Riwayat(
        id: data['id'] as int,
        kecamatanId: data['kecamatan_id'],
        qty: data['qty'] as num,
        createdAt: data['created_at'] as DateTime,
      );
}

final dummyHistory = [
  Riwayat(id: 1, qty: 20, createdAt: DateTime.now()),
  Riwayat(id: 2, qty: 40, createdAt: DateTime.now()),
  Riwayat(id: 3, qty: 50, createdAt: DateTime.now()),
];
