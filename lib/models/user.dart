part of 'models.dart';

class User {
  int? id;
  String? name;
  String? imgUrl;
  String? email;

  static String? token;

  User({
    this.id,
    this.name,
    this.imgUrl,
    this.email,
  });

  factory User.fromJson(Map<String, dynamic> data) => User(
        id: data['id'] as int,
        name: data['name'] as String,
        email: data['email'] as String,
        imgUrl: data['avatar'] as String,
      );
}
